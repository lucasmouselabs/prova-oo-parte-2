package model;

public class Paciente extends Usuario {
	private String tipoSanguineo;
	private String doenca;
	private double peso;
	private double altura;
	private int idade;
	private double valorDaConsulta;
	private int gravidadeDoProblema;// 1 a 5 
	private String nacionalidade;
	private String medicotratar;
	
	public String getTipoSanguineo() {
		return tipoSanguineo;
	}
	public void setTipoSanguineo(String tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}
	public String getDoenca() {
		return doenca;
	}
	public void setDoenca(String doenca) {
		this.doenca = doenca;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(double altura) {
		this.altura = altura;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public double getValorDaConsulta() {
		return valorDaConsulta;
	}
	public void setValorDaConsulta(double valorDaConsulta) {
		this.valorDaConsulta = valorDaConsulta;
	}
	public int getGravidadeDoProblema() {
		return gravidadeDoProblema;
	}
	public void setGravidadeDoProblema(int gravidadeDoProblema) {
		this.gravidadeDoProblema = gravidadeDoProblema;
	}
	public String getNacionalidade() {
		return nacionalidade;
	}
	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	public String getMedicotratar() {
		return medicotratar;
	}
	public void setMedicotratar(String medicotratar) {
		this.medicotratar = medicotratar;
	}

	
}
