package view;

import java.awt.EventQueue;
import model.*;
import controller.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JSlider;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Tela {

	private JFrame frame;
	private JTextField tf_nome;
	private JTextField tf_cpf;
	private JTextField tf_altura;
	private JTextField tf_peso;
	private JTextField tf_sangue;
	private JTextField tf_nacionalidade;
	private JTextField tf_data;
	private JTextField tf_doenca;
	private JTextField tf_medico;
	private JPasswordField pwdMedico;
	private JTextField tf_email;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tela window = new Tela();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Tela() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setEnabled(false);
		frame.setBounds(100, 100, 694, 546);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Tela.class.getResource("/view/Medicina-em-png-queroimagem-Ceiça-Crispim (2).png")));
		lblNewLabel.setBounds(12, 0, 128, 128);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Droid Sans Fallback", Font.BOLD, 14));
		lblNome.setBounds(148, 26, 70, 15);
		frame.getContentPane().add(lblNome);
		
		tf_nome = new JTextField();
		tf_nome.setEditable(false);
		tf_nome.setBounds(148, 45, 239, 19);
		frame.getContentPane().add(tf_nome);
		tf_nome.setColumns(10);
		
		JLabel lblCpf = new JLabel("Cpf");
		lblCpf.setFont(new Font("Droid Sans Fallback", Font.BOLD, 14));
		lblCpf.setBounds(407, 27, 70, 15);
		frame.getContentPane().add(lblCpf);
		
		tf_cpf = new JTextField();
		tf_cpf.setEditable(false);
		tf_cpf.setBounds(417, 45, 149, 19);
		frame.getContentPane().add(tf_cpf);
		tf_cpf.setColumns(10);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		lblAltura.setBounds(148, 76, 70, 15);
		frame.getContentPane().add(lblAltura);
		
		JLabel lblPeso = new JLabel("Peso");
		lblPeso.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		lblPeso.setBounds(230, 76, 70, 15);
		frame.getContentPane().add(lblPeso);
		
		JLabel lblTipoSanguneo = new JLabel("Tipo Sanguíneo");
		lblTipoSanguneo.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		lblTipoSanguneo.setBounds(297, 76, 121, 15);
		frame.getContentPane().add(lblTipoSanguneo);
		
		JLabel lblSexo = new JLabel("Sexo");
		lblSexo.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		lblSexo.setBounds(427, 76, 70, 15);
		frame.getContentPane().add(lblSexo);
		
		tf_altura = new JTextField();
		tf_altura.setEditable(false);
		tf_altura.setBounds(148, 90, 56, 19);
		frame.getContentPane().add(tf_altura);
		tf_altura.setColumns(10);
		
		tf_peso = new JTextField();
		tf_peso.setEditable(false);
		tf_peso.setBounds(230, 90, 56, 19);
		frame.getContentPane().add(tf_peso);
		tf_peso.setColumns(10);
		
		tf_sangue = new JTextField();
		tf_sangue.setEditable(false);
		tf_sangue.setBounds(307, 90, 56, 19);
		frame.getContentPane().add(tf_sangue);
		tf_sangue.setColumns(10);
		
		JRadioButton rbutton1 = new JRadioButton("Masculino");
		rbutton1.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		rbutton1.setBounds(469, 72, 115, 15);
		frame.getContentPane().add(rbutton1);
		
		JRadioButton rbutton2 = new JRadioButton("Feminino");
		rbutton2.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		rbutton2.setBounds(469, 88, 115, 15);
		frame.getContentPane().add(rbutton2);
		
		JLabel lblNacionalidade = new JLabel("Nacionalidade");
		lblNacionalidade.setFont(new Font("Droid Sans Fallback", Font.BOLD, 13));
		lblNacionalidade.setBounds(148, 121, 138, 15);
		frame.getContentPane().add(lblNacionalidade);
		
		JLabel lblDataDeNascimento = new JLabel("Data de Nascimento");
		lblDataDeNascimento.setBounds(348, 121, 149, 15);
		frame.getContentPane().add(lblDataDeNascimento);
		
		tf_nacionalidade = new JTextField();
		tf_nacionalidade.setEditable(false);
		tf_nacionalidade.setBounds(148, 136, 143, 19);
		frame.getContentPane().add(tf_nacionalidade);
		tf_nacionalidade.setColumns(10);
		
		tf_data = new JTextField();
		tf_data.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		tf_data.setEditable(false);
		tf_data.setBounds(348, 136, 129, 19);
		frame.getContentPane().add(tf_data);
		tf_data.setColumns(10);
		
		JCheckBox chckbxInternar = new JCheckBox("Internar");
		chckbxInternar.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		chckbxInternar.setBounds(511, 117, 129, 15);
		frame.getContentPane().add(chckbxInternar);
		
		JCheckBox chckbxConsultar = new JCheckBox("Consultar");
		chckbxConsultar.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		chckbxConsultar.setBounds(511, 138, 129, 15);
		frame.getContentPane().add(chckbxConsultar);
		
		JLabel lblNewLabel_1 = new JLabel("Email");
		lblNewLabel_1.setFont(new Font("Droid Sans Fallback", Font.BOLD, 15));
		lblNewLabel_1.setBounds(58, 164, 70, 15);
		frame.getContentPane().add(lblNewLabel_1);
		
		tf_doenca = new JTextField();
		tf_doenca.setEditable(false);
		tf_doenca.setBounds(58, 239, 149, 19);
		frame.getContentPane().add(tf_doenca);
		tf_doenca.setColumns(10);
		
		JSlider slider = new JSlider();
		slider.setBounds(37, 259, 200, 16);
		frame.getContentPane().add(slider);
		
		JLabel label = new JLabel("0");
		label.setBounds(26, 270, 70, 15);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("5");
		label_1.setBounds(240, 270, 70, 15);
		frame.getContentPane().add(label_1);
		
		JLabel lblNewLabel_3 = new JLabel("Médico Responsável");
		lblNewLabel_3.setFont(new Font("Droid Sans Fallback", Font.BOLD, 13));
		lblNewLabel_3.setBounds(58, 297, 146, 15);
		frame.getContentPane().add(lblNewLabel_3);
		
		tf_medico = new JTextField();
		tf_medico.setBounds(58, 324, 179, 19);
		frame.getContentPane().add(tf_medico);
		tf_medico.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Senha");
		lblNewLabel_4.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		lblNewLabel_4.setBounds(58, 360, 70, 15);
		frame.getContentPane().add(lblNewLabel_4);
		
		pwdMedico = new JPasswordField();
		pwdMedico.setToolTipText("LebronJames06");
		pwdMedico.setBounds(57, 373, 180, 19);
		frame.getContentPane().add(pwdMedico);
		
		JButton btnNewButton = new JButton("Autenticar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				autenticar();
				
			}
		});
		btnNewButton.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		btnNewButton.setBounds(86, 404, 117, 25);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Adicionar");
		btnNewButton_1.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				adicionar();
				
			}
		});
		btnNewButton_1.setBounds(44, 441, 107, 25);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Remover");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				remover();
			}
		});
		btnNewButton_2.setFont(new Font("Droid Sans Fallback", Font.BOLD, 12));
		btnNewButton_2.setBounds(169, 441, 107, 25);
		frame.getContentPane().add(btnNewButton_2);
		
		tf_email = new JTextField();
		tf_email.setEditable(false);
		tf_email.setBounds(58, 191, 190, 19);
		frame.getContentPane().add(tf_email);
		tf_email.setColumns(10);
		
		JLabel lblDoenaEGravidade = new JLabel("Doenca e Gravidade");
		lblDoenaEGravidade.setFont(new Font("Droid Sans Fallback", Font.BOLD, 13));
		lblDoenaEGravidade.setBounds(58, 212, 146, 15);
		frame.getContentPane().add(lblDoenaEGravidade);
		
		JButton btnListar = new JButton("Listar");
		btnListar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lista();
			}
		});
		btnListar.setFont(new Font("Droid Sans Fallback", Font.BOLD, 13));
		btnListar.setBounds(427, 167, 117, 25);
		frame.getContentPane().add(btnListar);
		
		textField = new JTextField();
		textField.setBounds(307, 210, 333, 256);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
	}

	protected void lista() {
		ControladorHospital umControlador = new ControladorHospital();
		umControlador.exibirP();
		
		
	}

	protected void remover() {
		// TODO Auto-generated method stub
		ControladorHospital umControlador = new ControladorHospital();
		
		
		String nome = tf_nome.getText();
		
		Paciente teste = new Paciente();
		
		teste = umControlador.buscarP(nome);
		umControlador.removerP(teste);
		
		JOptionPane.showMessageDialog(null, "Removido com Sucesso");
		
	}

	protected void adicionar() {
		ControladorHospital umControlador = new ControladorHospital();
		
		
		Paciente umPaciente = new Paciente();
		String nome = tf_nome.getText();
		umPaciente.setNome(nome);
		
		String cpf = tf_cpf.getText();
		umPaciente.setCpf(cpf);
		
		String altura = tf_altura.getText();
		double umAltura = Double.parseDouble(altura);
		umPaciente.setAltura(umAltura);
		
		String peso = tf_peso.getText();
		double umPeso = Double.parseDouble(peso);
		umPaciente.setPeso(umPeso);
		
		String tipoSanguineo = tf_sangue.getText();
		umPaciente.setTipoSanguineo(tipoSanguineo);
		
		String nacionalidade = tf_nacionalidade.getText();
		umPaciente.setNacionalidade(nacionalidade);
		
		String doenca = tf_doenca.getText();
		umPaciente.setDoenca(doenca);
		
		
		umControlador.addP(umPaciente);
		
		Medico umMedico = new Medico();
		
		String outronome = tf_medico.getText();
		umMedico.setNome(outronome);
		umControlador.addM(umMedico);
		
		umPaciente.setMedicotratar(outronome);
		
		JOptionPane.showMessageDialog(null, "Adicionado com Sucesso");
		
		
		
	}

	public void autenticar() {
		
		
		
		String senha = "LebronJames06";
		String tentar = pwdMedico.toString();
		if (senha == tentar){
			System.out.println("senha nem foi colocada");
			
		}else{
			
			
			
			
			JOptionPane.showMessageDialog(null, "Autenticado");
			tf_nome.setEditable(true);
			tf_cpf.setEditable(true);
			tf_altura.setEditable(true);
			tf_peso.setEditable(true);
			tf_sangue.setEditable(true);
			tf_nacionalidade.setEditable(true);
			tf_data.setEditable(true);
			tf_doenca.setEditable(true);
			tf_email.setEditable(true);
			
		
			
			
		}
	}
}
